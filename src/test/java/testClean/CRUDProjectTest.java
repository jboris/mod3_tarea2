package testClean;

import factoryRequest.FactoryRequest;
import factoryRequest.RequestInformation;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import utils.ConfigAPI;
import utils.ProjectProperties;

import java.io.IOException;

public class CRUDProjectTest {

    @BeforeEach
    public void setUp() throws IOException {
        new ProjectProperties().get();
    }
    @Test
    public void testCRUD() {
        JSONObject body = new JSONObject();
        body.put("Content", "Boris Item");
        RequestInformation requestInformation = new RequestInformation(ConfigAPI.CREATE_ITEM, body.toString());
        Response response = FactoryRequest.make(FactoryRequest.POST).send(requestInformation);
        response.then()
                .statusCode(200)
                .body("Content", equalTo("Boris Item"));
        String id = response.then().extract().path("Id").toString();

        body.put("Checked", true);
        requestInformation = new RequestInformation(ConfigAPI.UPDATE_ITEM.replace("ID", id), body.toString());
        response = FactoryRequest.make(FactoryRequest.PUT).send(requestInformation);
        response.then()
                .statusCode(200)
                .body("Checked", equalTo(true));

        requestInformation = new RequestInformation(ConfigAPI.READ_ITEM.replace("ID", id), "");
        response = FactoryRequest.make(FactoryRequest.GET).send(requestInformation);
        response.then()
                .statusCode(200)
                .body("Content", equalTo("Boris Item"));

        requestInformation = new RequestInformation(ConfigAPI.DELETE_ITEM.replace("ID",id),"");
        response = FactoryRequest.make(FactoryRequest.DELETE).send(requestInformation);
        response.then()
                .statusCode(200)
                .body("Deleted", equalTo(true));
    }
}
