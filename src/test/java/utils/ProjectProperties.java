package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProjectProperties {
    public void get() throws IOException {
        Properties properties = new Properties();
        String route = "qa.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(route);
        if (inputStream != null)
            properties.load(inputStream);
        ConfigEnv.host = properties.getProperty("host");
        ConfigEnv.user = properties.getProperty("user");
        ConfigEnv.password = properties.getProperty("password");
        inputStream.close();
    }
}
